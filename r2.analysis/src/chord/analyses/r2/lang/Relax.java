package chord.analyses.r2.lang;

public class Relax {
	public static boolean relax(boolean b){return b;}
	public static float relax(float f){return f;}
	public static int relax(int i){return i;}
	public static double relax(double d){return d;}
	public static byte relax(byte b){return b;}
	public static char relax(char c){return c;}
	public static short relax(short s){return s;}
	public static long relax(long l){return l;}
	
	//TODO generate enough possible relax_all
	public static int[] relax_all_FIELD2_TAG1(int[] arr) {return arr;}
	public static int[] relax_all_FIELD1_TAG11(int[] arr) {return arr;}
	public static int[] relax_all_FIELD1_TAG2(int[] arr) {return arr;}
	public static int[] relax_all_FIELD1_TAG5(int[] arr) {return arr;}
	public static float[] relax_all_FIELD1_TAG6(float[] arr) {return arr;}
	public static double[] relax_all_FIELD1_TAG1(double[] arr){return arr;}
}
